#
# This policy configuration will be used by all products that
# inherit from Calyx
#

BOARD_PLAT_PRIVATE_SEPOLICY_DIR += \
    vendor/calyx/sepolicy/common/private

BOARD_PLAT_PUBLIC_SEPOLICY_DIR += \
    vendor/calyx/sepolicy/common/public
